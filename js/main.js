
$(function(){

    //wrapInner()
    // umgibt Content eines Elementes....

    // $("li").wrapInner("<a></a>");
    // $("li").wrapInner("<a><span></span></a>");
    $("li").wrapInner(function() {
        return "<a href=\"" + $(this).text() + "\"></a>"
    });

    // wrap()

    $("ul").wrap("<div></div>");

    // unwrap()
    $("ul").unwrap();

    // wrapAll()
    // Elemente werden dafür ....

    $("p").wrapAll("<div></div>");

    // clone(deep)
    //  erstellt Kopien 

    $("ul").clone(false).appendTo("body");
    
    $("input").val("Wert im Input Field");



});

/*
    Selektoren $() und jQuery()
    konfliktloser Modus:    jQuery.noConflict()
    im konfliktlosen Modus ist $() nicht mehr nutzbar

    einleitender Handler, der darauf wartet, dass DOM fertig geladen ist
    $(document).ready(function() {})
    jQuery(function() {})
*/

$(function() {

    console.log($("#url_link"));
    console.log(document.querySelector("#url_link"));

    console.log($("#url_link") == document.querySelector("#url_link"));
    console.log($("#url_link")[0] == document.querySelector("#url_link"));
    
    // Attribute lesen, schreiben, ändern
    $("#url_link").attr("title","Ab jetzt mit jQuery");
    console.log($("#url_link").attr("title"));
    $("#url_link").attr({
        title:  "jQuery",
        href:   "https://jquery.com"
    });
    console.log($("#url_link").attr("title"));
    console.log($("#url_link").attr("href"));

    // Attribute entfernen
    $("#url_link").removeAttr("title");
    console.log($("#url_link").attr("title")); // undefined


    // Zugriff auf Content

    // incl. Markups
    let contentH1 = $("h1").html();
    console.log(contentH1);

    let contentPara = $("p").html();
    console.log(contentPara);

    $("h1").html(contentPara);
    // $("p").html(contentH1);
    // $("p:first").html(contentH1);
    $("p").first().html(contentH1);

    // ohne Markups
    $("p").first().text("<b>Hallo. Ich bin ein Text</b>");
    // console.log($("p").text());
    console.log($("p").first().text());

    /*
        für ältere jQuery-Versionen
        :first              - 1. Element
        :last               - letztes Element
        :even               - alle Elemente an gerader Index-Position
        :odd                - alle Elemente an ungerader Index-position
        :eq(index)          - Element an übergebenr Index-Position
        :lt(index)          - Elemente vor Index-position
        :gt(index)          - Elemente nach Index-Position
        
        :nth-child(position) - Element an Position (index + 1)
    */

    // neuere jQuery-Versionen
    console.log($("p"));

    // Element an Index-Position 2
    console.log($("p").eq(2));
    // erstes Element
    console.log($("p").first());
    // letztes Element
    console.log($("p").last());

    console.log($("li"));
    // Auswahl der Elemente, die eine Bedingung erfüllen (beinhalten)
    console.log($("li").has("ul"));
    // Auswahl filtern
    console.log($("li").filter(".firstElement"));
    // Auswahl der Elemente, die eine Bedingung nicht erfüllen
    console.log($("li").not(".firstElement"));

    // Bereich von Elementen
    // slice(indexStart, indexEnd) -> indexEnd nicht enthalten
    console.log($("p").slice(0,3));


    // Content erzeugen
    $("div.element").before("<p>before text</p>");
    $("div.element").prepend("<p>prepend text</p>");
    $("div.element").append("<p>append text</p>");
    $("div.element").after("<p>after text</p>");

    $("#email").after("<br><span class=\"fehler>\">Mail-Adressefehlerhaft</span>");

    //Inline-styles setzen und anliegende styles lesen
    console.log($("p").css("font-size"));   //16px
    $("p").css("background-color", "yellow");
    console.log($("p").css("background-color"));

    $("p").css({
        backgroundColor: "orange",
        padding: "10px 20px",
        border: "1px solid green"
    });

    // Arbeit mit klassen
    // hinzufügen
    
    $("p").addClass("fehler");
    //entfernen
    $("p").removeClass("fehler");
    $("p").toggleClass("fehler");   // hinzufügen, da nicht vorhanden

    //prüfen

    if($("p").hasClass("fehler")) {
        $("p").toggleClass("fehler");   // entfernt, da vorhanden
    }


    // Events

    $("p").on("click", function(){
        alert("Da hat doch");
    });
    $("p").on("click", function(){
        alert("Da hat doch.......................");
    });

    //mit off() alle mit on() registierten Handler für ein übergebenes .....
    /* $("p").off("click");

    $(".bild").mouseover(function(){
        $(".bild").attr("src", "./img/2.jpg");
    });
    $(".bild").mouseout(function(){
        $(".bild").attr("src", "./img/1.jpg");
    }); */

    $(".bild").hover(
        function(){
            $(this).attr("src","./img/2.jpg");
        },
        function(){
            $(this).attr("src","./img/1.jpg");
        }
        
    );

    // vordefinierte Effekte
        $("#bild-ausblenden").on("click", function(){
            // Bild ausblenden
            $(".bild").hide("fast");    //$(".bild").hide("slow"); $(".bild").hide(5000);
            return false;
        });

        $("#bild-einblenden").on("click", function(){
            // Bild einblenden
            $(".bild").show("slow");    //$(".bild").hide("slow"); $(".bild").hide(5000);
            return false;
        });

        //fadeIn(), fadeOut(), slideUp(), slideDown()

        $("#bild-toggle").on("click", function(){
            $(".bild").toggle("fast");
            return false;
        });

        // Eigene Effekte 
        $("button#animate").on("click", function(){
            $("div#container").animate({
                width: "450px",
                fontSize: "4em",
                opacity: .5
            },2500);
        });

        let styleStart = {
            position:   "relative",
            left:   "-2000px",
            opacity:    0,
            backgroundColor:    "blue",
            color:  "#ffffff",
            padding:    "10px 20px"
        };

        let styleEnd = {
            left:   0,
            opacity: 1
        };
        $("div.element").css(styleStart);

        $("div.element").animate(styleEnd,3500);

        // Navigation im Dom

        // Kindelemente

        console.log($("ul").children());        //kindelemente
        console.log($("article").find("a"));    // 1. Linl im Artikle

        //Elternelemente
        console.log($("li").parent());          // direct Elternelemente
        console.log($("li").parents());         // alle eltern elemente

        console.log($("li").parentsUntil("body"));      //Alle elemente bis exclusive dem übergebenen Element
        console.log($("li").closest("article"));        // 1. Ele

        // Geschister element
        console.logen($("h2").next("a"));   // Folgende Geschwister element.....
        console.log($("h2").nextAll());     //alle folgende Geschwisterelemente
        console.log($("h2").nextAll("a"));  // alle folgende geschwisterelemente, die eine übergebene Bedingung erfüllen
        console.log($("h2").nextUntil("a"));    // alle folgende Geschwisterelemente bis zu einem bestimten Element (exclusive)

        console.log($("ul").prev());    // direkt vorherndes Geschwisterelemente
        console.log($("ul").prevAll()); // alle vorhergehendes Geschwister
        //weiter methoden wie bei next()


        console.log($("ul").siblings());    // alle Geschwisterelemente


        $("h2").on("click", function(){
            $(this).next().toggle("slow");
        }).next().hide();




});
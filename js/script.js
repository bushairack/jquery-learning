$(function() {

/* 

    Ajax mit jQuery

    $.ajax()    - beliebige asynchrone Anfragen
    $.get()     - asynchrone GET anfragen
    $.getJSON() - asynchrone GET anfragen, um JSON-strings zu laden
    $.post()    - asynchrone POST anfragen

    [element].load()    - asynchrone GET anfragen, um HTML - Snippets zu laden
                        - Methode wird auf Elemente angewendet
                        - HTML -Snippet wird als Content an Element übergeben
*/
let login = $("#login");
let register = $("#register");
let container = $("#main-content");


login.on("click", function() {
    loadContent("login");
    return false;
});

register.on("click", function() {
    loadContent("register");
    return false;
});

function loadContent(linkName) {
/*     // first possiblity with success and error =>(integratin)
    $.ajax({
        url:    linkName + ".html",
        type:   "GET",
        dataType:   "html" , // json, xml eath data aano transfer chezyyendath
        success:    function(data) {
                    container.html(data);
                    },
        error:      function(xhrObject, error, errorMessage){
                    console.log(xhrObject);
                    console.log(error, errorMessage);
        }
    }) */;

    // second possiblity with done() and fail()

    /* $.get({
        url:    linkName + ".html",
        dataType:   "html"
    }).done(function(data) {
        console.log(data);
    }).fail(function(xhrObject, error, errorMessage) {
        console.log(xhrObject);
        console.log(error, errorMessage);
    }); */

    // third posiblitty with load() for Snippets

    container.load(linkName + ".html", 
    function() {    // function is optional
        // wird ausgeführt, wenn Snippet integriert ist

        console.log("Habe fertig");
    }
    );
}

});

$(function(){
    const bilder = ["1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg"];
    for(let i=0; i<bilder.length; i++){
            $("#container").append('<div><img src="./img/' + bilder[i] +'" </div>');
    }
    // $("#container > div:gt(0)").hide();
    // $("#container > div").hide().first.show();
    $("#container > div").slice(1).hide();

    $("#container").prepend('<a class="prev"><</a>');
    $("#container").prepend('<a class="next">></a>');

    $("#container .prev").on("click", function(){
        $("#container > div")
        .first()
        .fadeOut(2000)
        .end()
        .last()
        .fadeIn(2000)
        .prependTo('#container');
    });
    $("#container .next").on("click", function(){
        $("#container > div")
        .first()
        .fadeOut(2000)
        .next()
        .fadeIn(2000)
        .prev()
        .appendTo("#container");
    });
 $("h1").css({
    marginTop: "50px",
    textAlign: "center"
});
$("#container").css({
    margin: "30px auto 0",
    position: "relative",
    width: "800px",
    height: "400px",
    padding: "10px 10px 0",
    boxShadow: "0 -60px 100px rgba(0,0,0,.2)"
});
$("#container > div").css({
    position:   "absolute",
    top:    "10px",
    left:   "10px",
    bottom: 0,
    right:  "10px"
});
$("hr").css({
    height: "2px",
    border: "none",
    backgroundImage: "linear-gradient(to right, rgba(180,180,180,0), rgba(180,180,180,1), rgba(180,180,180,0))",
    maxwidth:   "1000px",
    margin: "-1px auto 0"
});
$(".next, .prev").css({
    background: "rgba(180,180,180,.75)",
    position:   "absolute",
    "z-index":      999,
    padding:    "5px 10px",
    borderRadius:    "50%",
    top:        "48%",
    fontSize:   "1.25rem",
    cursor:     "pointer"
});
$(".next").css("right", "20px");
$(".prev").css("left", "20px");
});